import React, { Component } from 'react'
import {View,Text,TextInput,TouchableOpacity} from 'react-native'
import {db}  from './database/confg'

const addItem = (proName,proPrice) => {
  db.ref('items/Products').push({
    productName: proName,
    productPrice:proPrice
  });
};

export default class App extends Component { 
  constructor(props){
    super(props)
    this.state={
      productName:'',
      productPrice:''
    }
  }
  handleSubmit=()=>{
    addItem(this.state.productName,this.state.productPrice)
  }
  render() {
    return (
      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        <View style={{flex:0.1,justifyContent:'center',alignItems:'center'}}>
          <TextInput 
              style={{
                width: 350,
                height: 40,
                fontSize:15
                }}
              placeholder={'Product_Price'} 
              placeholderTextColor={'grey'}
              underlineColorAndroid={'black'}
              onChangeText={(value)=>this.setState({productName:value})}
              />
        </View>
        <View style={{flex:0.1,justifyContent:'center',alignItems:'center'}}>
            <TextInput 
            style={{
              width: 350,
              height: 40,
              fontSize:15
              }}
            placeholder={'Product_Price'} 
            placeholderTextColor={'grey'}
            underlineColorAndroid={'black'}
            onChangeText={(value)=>this.setState({productPrice:value})}
            />
        </View>
        <View style={{flex:0.1,justifyContent:'center',alignItems:'center'}}>
          <TouchableOpacity onPress={this.handleSubmit} style={{backgroundColor:"#36C5F0",width:350,height:35,justifyContent:'center',alignItems:'center'}}>
            <Text style={{fontSize:15}}>Submit</Text>
          </TouchableOpacity>
          </View>
      </View>
    )
  }
}
